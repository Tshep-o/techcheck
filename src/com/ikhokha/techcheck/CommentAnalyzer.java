package com.ikhokha.techcheck;

import com.ikhokha.techcheck.metrics.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class CommentAnalyzer implements Callable<Map<String, Integer>> {
    private File file;

    public CommentAnalyzer(File file) {
        this.file = file;
    }

    private Map<String, Integer> analyze() {
        Map<String, Integer> resultsMap = new HashMap<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line = null;

            while ((line = reader.readLine()) != null) {
                ShorterThan15 shorterThan15 = new ShorterThan15(line, resultsMap);
                shorterThan15.execute();
                MoverMentions moverMentions = new MoverMentions(line, resultsMap);
                moverMentions.execute();
                ShakerMentions shakerMentions = new ShakerMentions(line, resultsMap);
                shakerMentions.execute();
                Questions questions = new Questions(line, resultsMap);
                questions.execute();
                Spam spam = new Spam(line, resultsMap);
                spam.execute();
            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + file.getAbsolutePath());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO Error processing file: " + file.getAbsolutePath());
            e.printStackTrace();
        }

        return resultsMap;
    }

    @Override
    public Map<String, Integer> call() throws Exception {
        return this.analyze();
    }
}

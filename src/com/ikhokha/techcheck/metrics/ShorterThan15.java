package com.ikhokha.techcheck.metrics;

import java.util.Map;

public class ShorterThan15 implements IMetric {
    private final String text;
    private final Map<String, Integer> countMap;

    public ShorterThan15(String text, Map<String, Integer> countMap) {
        this.text = text;
        this.countMap = countMap;
    }

    @Override
    public void execute() {
        if (text.length() < 15) {
            countMap.putIfAbsent("SHORTER_THAN_15", 0);
            countMap.computeIfPresent("SHORTER_THAN_15", (k, v)-> v + 1);
        }
    }
}
package com.ikhokha.techcheck.metrics;

public interface IMetric {
    void execute();
}

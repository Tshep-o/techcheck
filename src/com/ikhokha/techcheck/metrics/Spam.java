package com.ikhokha.techcheck.metrics;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Spam implements IMetric {
    private final String text;
    private final Map<String, Integer> countMap;

    public Spam(String text, Map<String, Integer> countMap) {
        this.text = text;
        this.countMap = countMap;
    }

    @Override
    public void execute() {
        Pattern r = Pattern.compile("www\\.|http://|https://|[a-z0-9]*\\.[a-z0-9]*\\.");
        Matcher m = r.matcher(text);

        if(m.find()){
            countMap.putIfAbsent("SPAM", 0);
            countMap.computeIfPresent("SPAM", (k, v) -> v + 1);
        }

    }
}

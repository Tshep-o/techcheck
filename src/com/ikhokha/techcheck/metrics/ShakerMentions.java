package com.ikhokha.techcheck.metrics;

import java.util.Map;

public class ShakerMentions implements IMetric {
    private final String text;
    private final Map<String, Integer> countMap;

    public ShakerMentions(String text, Map<String, Integer> countMap) {
        this.text = text;
        this.countMap = countMap;
    }

    @Override
    public void execute() {

        if (text.contains("Shaker")) {
            countMap.putIfAbsent("SHAKER_MENTIONS", 0);
            countMap.computeIfPresent("SHAKER_MENTIONS", (k, v) -> v + 1);
        }
    }
}

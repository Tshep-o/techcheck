package com.ikhokha.techcheck.metrics;

import java.util.Map;

public class MoverMentions implements IMetric {
    private final String text;
    private final Map<String, Integer> countMap;

    public MoverMentions(String text, Map<String, Integer> countMap) {
        this.text = text;
        this.countMap = countMap;
    }

    @Override
    public void execute() {
        if (text.contains("Mover")) {
            countMap.putIfAbsent("MOVER_MENTIONS", 0);
            countMap.computeIfPresent("MOVER_MENTIONS", (k, v) -> v + 1);
        }
    }
}

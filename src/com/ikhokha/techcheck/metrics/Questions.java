package com.ikhokha.techcheck.metrics;

import java.util.Map;

public class Questions implements IMetric {
    private final String text;
    private final Map<String, Integer> countMap;

    public Questions(String text, Map<String, Integer> countMap) {
        this.text = text;
        this.countMap = countMap;
    }

    @Override
    public void execute() {

        long count = text.chars().filter(ch -> ch == '?').count();
        countMap.putIfAbsent("QUESTIONS", 0);
        countMap.computeIfPresent("QUESTIONS", (k, v) -> v + (int)count);
    }
}

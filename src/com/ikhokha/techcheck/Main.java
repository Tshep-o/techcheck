package com.ikhokha.techcheck;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class Main {

    public static void main(String[] args) {
        Map<String, Integer> totalResults = new HashMap<>();
        File docPath = new File("docs");
        File[] commentFiles = docPath.listFiles((d, n) -> n.endsWith(".txt"));

        ThreadPoolExecutor executorService = new ThreadPoolExecutor(
                1,
                Runtime.getRuntime().availableProcessors() * 8,
                1000, TimeUnit.MILLISECONDS,
                new ArrayBlockingQueue<Runnable>( (int) Math.ceil(commentFiles.length * 0.2) )
        );

        executorService.setRejectedExecutionHandler(new RejectedExecutionHandler() {
            public void rejectedExecution(Runnable runnable, ThreadPoolExecutor executor) {
                try {
                    executor.getQueue().put(runnable);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        List<Future> futureList = new ArrayList<>();

        for (File commentFile : commentFiles) {
            CommentAnalyzer commentAnalyzer = new CommentAnalyzer(commentFile);
            Future<Map<String, Integer>> future = executorService.submit(commentAnalyzer);
            futureList.add(future);
        }

        for (Future future : futureList) {
            try {
                addReportResults((Map<String, Integer>) future.get(), totalResults);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        executorService.shutdown();
        System.out.println("RESULTS\n=======");
        totalResults.forEach((k, v) -> System.out.println(k + " : " + v));
    }


    /**
     * This method adds the result counts from a source map to the target map
     *
     * @param source the source map
     * @param target the target map
     */
    private static void addReportResults(Map<String, Integer> source, Map<String, Integer> target) {

        for (Map.Entry<String, Integer> entry : source.entrySet()) {
            target.putIfAbsent(entry.getKey(), 0);
            target.computeIfPresent(entry.getKey(), (k, v) -> target.get(k) + entry.getValue());
        }

    }

}
